require("dotenv").config();
const { MONGO_URI } = process.env;

// MONGODB
const mongoose = require('mongoose');

exports.connect = () => {
  // Connecting to the database
  mongoose
    .connect(MONGO_URI, { useNewUrlParser: true })
    .then(() => {
      console.log("Successfully connected to MongoDB");
    })
    .catch((error) => {
      console.log(`MongoDB connection failed. exiting now... : ${error}`);
      process.exit(1);
    });
};