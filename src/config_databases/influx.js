require("dotenv").config();
const { INFLUX_URI } = process.env;

// INFLUXDB
const Influx = require('influx');
// Connect to a single host with a DSN:
const influx = new Influx.InfluxDB(INFLUX_URI);

exports.connect = () => {
  influx
    .ping(5000).then(hosts => {
    hosts.forEach(host => {
      if (host.online) {
        console.log("Successfully connected to InfluxDB");
        console.log(`${host.url.host} responded in ${host.rtt}ms running ${host.version}`)
      } else {
        console.log("InfluxDB connection failed. exiting now...");
        console.log(`${host.url.host} is offline :(`)
        process.exit(1);
      }
    })
  })
};