const influxController = require("../controllers/influx.controller.js");
const router = require("express").Router();

// #Region appel vers InfluxDB

    // Check test first query
    // router.get("/cs", ihm.queryCS);

    // Check test first query
    // router.get("/cs/:id", ihm.queryCSbyID);

    // Check test first query
    // router.get("/ss", ihm.querySS);

    // Check test first query
    //router.get("/ss/:id", ihm.querySSbyID);

    // Retrieve all from Influx measurements
    router.get("/all", influxController.findAllInflux);
    
    // Calculation of brine creation
    router.get("/create-brine/:concentration/:volume", influxController.findBrineInfos);
    
    // Calculation of brine adjustment
    router.get("/adjust-brine/:concentrationP/:concentrationD/:volume", influxController.findAdjustBrineInfos);

// #endRegion appel vers InfluxDB

module.exports = router;
