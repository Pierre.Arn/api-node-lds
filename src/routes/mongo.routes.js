const mongoController = require("../controllers/mongo.controller.js");
const router = require("express").Router();

// #Region appel vers MongoDB

    // Retrieve all from Tanks collection
    router.get("/retrieve-tanks", mongoController.findAllTanks);

    // Retrieve all from Tanks_Brine collection
    router.get("/retrieve-tanks-brine", mongoController.findAllTanksBrine);

    // Retrieve all from Tanks_Compens collection     
    router.get("/retrieve-tanks-compens", mongoController.findAllTanksCompens);

    // Retrieve all from Configs collection     
    router.get("/config", mongoController.findConfigInfos);

    // Update a tank from Tanks collection with id
    router.put("/update-tank/:id", mongoController.updateTank);

    // Update a tank from Tanks_Brine collection with id
    router.put("/update-tank-brine/:id", mongoController.updateTankBrine);

    // Update a tank from Tanks_Compens collection with id
    router.put("/update-tank-compens/:id", mongoController.updateTankCompens);

    router.post("/register", mongoController.register)
    router.post("/login", mongoController.login)
    router.get("/welcome", mongoController.welcome)

// #endRegion appel vers MongoDB

module.exports = router;
