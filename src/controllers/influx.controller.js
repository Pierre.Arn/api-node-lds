require("dotenv").config();
const { INFLUX_URI, NBR_CUVES, PREMIERE_CUVE } = process.env;

// INFLUXDB
const Influx = require('influx');
// Connect to a single host with a DSN:
const influx = new Influx.InfluxDB(INFLUX_URI);

// Get all data from calibrated_sensor measurement
exports.queryCS = (req, res) => {
  influx.query('select * from calibrated_sensor where time > now() - 1m').then(results => {
    res.status(201).json(results);
  }).catch(err => {
    console.log(`failed: ${err.message}`);
    res.status(500).json(err);
  })
};

// Get data from calibrated_sensor measurement by cuve id
exports.queryCSbyID = (req, res) => {
  const id = req.params.id;
  influx.query(`select * from calibrated_sensor where time > now() - 1m and cuve='${id}'`).then(results => {
    res.status(201).json(results);
  }).catch(err => {
    console.log(`failed: ${err.message}`);
    res.status(500).json(err);
  })
};

// Get all data from smart_sensor measurement
exports.querySS = (req, res) => {
  influx.query('select * from smart_sensor where time > now() - 1m').then(results => {
    res.status(201).json(results);
  }).catch(err => {
    console.log(`failed: ${err.message}`);
    res.status(500).json(err);
  })
};

// Get data from smart_sensor measurement by cuve id
exports.querySSbyID = (req, res) => {
  const id = req.params.id;
  influx.query(`select * from smart_sensor where time > now() - 1m and cuve='${id}'`).then(results => {
    res.status(201).json(results);
  }).catch(err => {
    console.log(`failed: ${err.message}`);


    res.status(500).json(err);
  })
};

// Retrieve all Cuve from Influx
exports.findAllInflux = (req, res) => {
  influx.query('select TempCond_calibrated, TempCond_status, Temp_calibrated, Temp_status, cuve from calibrated_sensor where time > now() - 90s').then(resultsCS => {
    influx.query('select * from smart_sensor where time > now() - 90s').then(resultsSS => {
      var finalResult = [];
      for (let i = 0; i < NBR_CUVES; i++) {
        finalResult.push({
          cuve: `${i + parseInt(PREMIERE_CUVE)}`,
          temp: 404,
          tempStatus: 2,
          tempCond: 404,
          tempCondStatus: 2,
          ssd: "no",
          frozen: "--",
          infoStatus: "status-warnTemp",
          concentration: "--",
          etat: "D",
        });
      }
      resultsCS.forEach(elementCS => {
        const indexCS = elementCS.cuve - PREMIERE_CUVE;
        finalResult[indexCS].temp = Math.round(elementCS.Temp_calibrated * 10) / 10; // Température de la sonde de température
        finalResult[indexCS].tempStatus = elementCS.Temp_status; // État de la sonde de température
        finalResult[indexCS].tempCond = elementCS.TempCond_calibrated; // Température de la sonde de conductivité
        finalResult[indexCS].tempCondStatus = elementCS.TempCond_status; // État de la sonde de conductivité
      });
      resultsSS.forEach(elementSS => {
        let valueInfoStatus = "";
        switch (elementSS.infoStatus) {
          case 0:
            valueInfoStatus = "status-ok";
            break;
          case 1:
            valueInfoStatus = "status-dihydrate";
            break;
          case 2:
            valueInfoStatus = "status-glace";
            break;
          case 3:
            valueInfoStatus = "status-carter";
            break;
        }
        let valueState = "";
        switch (elementSS.state) {
          case 0:
            valueState = "V";
            break;
          case 1:
            valueState = "AS";
            break;
          case 2:
            valueState = "EM";
            break;
          case 3:
            valueState = "RSW";
            break;
          case 4:
            valueState = "S";
            break;
        }
        const indexSS = elementSS.cuve - PREMIERE_CUVE;
        finalResult[indexSS].ssd = elementSS.ShowSmartData;
        finalResult[indexSS].frozen = Math.round(elementSS.frozen * 100) / 100;
        finalResult[indexSS].infoStatus = valueInfoStatus;
        finalResult[indexSS].concentration = Math.round(elementSS.nacl * 100) / 100;
        finalResult[indexSS].etat = valueState;

      });
      var sortByCuveName = finalResult.slice(0);
      sortByCuveName.sort(function (a, b) {
        return a.cuve - b.cuve;
      });
      res.status(200).json(sortByCuveName);
    }).catch(err => {
      console.log(`failed: ${err.message}`);
      res.status(500).json(err);
    })
  }).catch(err => {
    console.log(`failed: ${err.message}`);
    res.status(500).json(err);
  });
};

exports.findBrineInfos = (req, res) => {
  const concentration = req.params.concentration;
  const volume = req.params.volume;
  const returnValues = getBrineInfos(concentration, volume)
  res.status(200).json(returnValues);
};

function getBrineInfos(concentration, volume) {
  const m_H20_kgl = 0.997047;
  const m_NACL_kgl = 0.036;

  // Get the density of the final brine
  const d_Naclg = compute_d_goal(concentration);
  // Compute the volume of EM
  const V_em = (volume * d_Naclg) / (m_H20_kgl + (concentration / (1 - concentration)) * m_H20_kgl);

  // Compute the weight of H2O in the tank from the EM
  const m_H2O = V_em * m_H20_kgl;
  // Compute the weight of Nacl in the tank from the EM
  const m_Nacl_em = V_em * m_NACL_kgl;
  // Compute the weight of Nacl to add in the tank
  const m_Nacl_add = ((concentration * m_H2O) / (1 - concentration)) - m_Nacl_em;
  // Get freezing temperature
  const tempCongel = freezing_temp(concentration);

  let volumeEM = Math.round(V_em * 100) / 100;
  let tonnageSel = Math.round(m_Nacl_add * 100) / 100;
  let nbrSacSel = 0;
  if (m_Nacl_add != 0) {
    nbrSacSel = Math.round(m_Nacl_add / 0.025);
  }
  const returnValues = [
    Math.round(d_Naclg * 1000) / 1000,
    volumeEM,
    tonnageSel,
    nbrSacSel,
    tempCongel
  ]
  return returnValues;
}

exports.findAdjustBrineInfos = (req, res) => {
  const concentrationP = req.params.concentrationP;
  const concentrationD = req.params.concentrationD;
  const volume = req.params.volume;

  const d_Naclg = compute_d_goal(concentrationD);
  const tonnageSel = Math.round(compute_m_Nacl_add(concentrationP, concentrationD, volume) * 100) / 100;
  const tempCongel = freezing_temp(concentrationD);
  let nbrSacSel = 0;
  if (tonnageSel != 0) {
    nbrSacSel = Math.round(tonnageSel / 0.025);
  }
  const returnValues = [
    Math.round(d_Naclg * 1000) / 1000,
    tonnageSel,
    nbrSacSel,
    tempCongel
  ]
  res.status(200).json(returnValues);
};

function compute_d_goal(concentration) {
  concentration = concentration * 100
  if (concentration <= 23.310) {
    // Approximation (see graph courbe.ods)
    return (((3 * 10 ** (-5)) * concentration * concentration) + 0.007 * concentration + 0.9985)
  } else {
    return (0.0084 * concentration + 0.9788)
  }
}

// Compute the weight of nacl to add in the tank
function compute_m_Nacl_add(Nacl_start, Nacl_goal, V_tank_start) {
  const m_H2O_start = compute_m_H20(Nacl_start)
  const m_Nacl_start = compute_m_Nacl(Nacl_start)
  const m_Nacl_add = ((m_H2O_start * V_tank_start * Nacl_goal) / (1 - Nacl_goal)) - m_Nacl_start * V_tank_start
  return (m_Nacl_add / 1000)
}

// Compute the weight of H2O in g/L for specific Nacl (see graph courbe.xlsx)
function compute_m_H20(Nacl_start) {
  // Convert Nacl in %
  if (Nacl_start < 1) {
    Nacl_start = Nacl_start * 100
  }
  return (-0.0542 * Nacl_start * Nacl_start - 2.9119 * Nacl_start + 998.17)
}

// Compute the weight of Nacl in g/L for specific Nacl (see graph courbe.xlsx)
function compute_m_Nacl(Nacl_start) {
  // Convert Nacl in %
  if (Nacl_start < 1) {
    Nacl_start = Nacl_start * 100
  }
  return (0.0807 * Nacl_start * Nacl_start + 9.8658 * Nacl_start + 0.227)
}

function freezing_temp(concentration) {
  let tempCongel;
  if (concentration <= 0.233) {
    tempCongel = (-8.93145 * Math.pow(10, -6) * Math.pow(concentration * 100, 5)) +
      (4.60069 * Math.pow(10, -4) * Math.pow(concentration * 100, 4)) -
      (8.32331 * Math.pow(10, -3) * Math.pow(concentration * 100, 3)) +
      (4.83253 * Math.pow(10, -2) * Math.pow(concentration * 100, 2)) -
      (7.03778 * Math.pow(10, -1) * (concentration * 100)) +
      (7.80035 * Math.pow(10, -3));
  } else {
    tempCongel = (-0.3604 * Math.pow(concentration * 100, 3)) +
      (26.242 * Math.pow(concentration * 100, 2)) -
      (628.95 * (concentration * 100))
      + 4945;
  }
  return Math.round(tempCongel * 100) / 100;
}