const Tank = require('../models/tank.model.js');
const TankBrine = require('../models/tankBrine.model.js');
const TankCompens = require('../models/tankCompens.model.js');
const Config = require('../models/config.model.js');

let errorMessage = "Something went wrong :(";

// Retrieve all Tanks from MongoDB.
exports.findAllTanks = (req, res) => {
  Tank.find()
    .sort({ 'numero': 1 })
    .exec()
    .then(tanks => res.status(200).json(tanks))
    .catch(err => res.status(500).json({
      message: errorMessage,
      error: err
    }));
};

// Retrieve all Tanks_Brine from MongoDB.
exports.findAllTanksBrine = (req, res) => {
  TankBrine.find()
    .sort({ 'numero': 1 })
    .exec()
    .then(tanks => res.status(200).json(tanks))
    .catch(err => res.status(500).json({
      message: errorMessage,
      error: err
    }));
};

// Retrieve Configs from MongoDB.
exports.findConfigInfos = (req, res) => {
  Config.find()
    .exec()
    .then(config => res.status(200).json(config))
    .catch(err => res.status(500).json({
      message: errorMessage,
      error: err
    }));
};

// Retrieve Tanks_Compens from MongoDB.
exports.findAllTanksCompens = (req, res) => {
  TankCompens.find()
    .exec()
    .then(tanks => res.status(200).json(tanks))
    .catch(err => res.status(500).json({
      message: errorMessage,
      error: err
    }));
};

// Update a tank from Tanks_Brine collection by the id in the request
exports.updateTankBrine = (req, res) => {
  const filter = req.params.id;
  const update = req.body;
  TankBrine.findOneAndUpdate({ TankNumber: filter }, update)
    .then(tank => res.status(200).json(tank))
    .catch(err => res.status(500).json({
      message: `${errorMessage}, tank brine with id ${filter} not found :(`,
      error: err
    }));
};

// Update a tank from Tanks_Compens collection by the id in the request
exports.updateTankCompens = (req, res) => {
  const filter = req.params.id;
  const update = req.body;
  TankCompens.findOneAndUpdate({ TankNumber: filter }, update)
    .then(tank => res.status(200).json(tank))
    .catch(err => res.status(500).json({
      message: `${errorMessage}, tank compens with id ${filter} not found :(`,
      error: err
    }));
};

// Update a tank from Tanks collection by the id in the request
exports.updateTank = (req, res) => {
  const filter = req.params.id;
  const update = req.body;
  Tank.findOneAndUpdate({ TankNumber: filter }, update)
    .then(tank => res.status(200).json(tank))
    .catch(err => res.status(500).json({
      message: `${errorMessage}, tank with id ${filter} not found :(`,
      error: err
    }));
};

// TOKEN JWT

const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const User = require("../models/user.model");
const auth = require("../middleware/auth");

// Register
exports.register = async (req, res) => {
  // our register logic goes here...
  try {
    // Get user input
    const { first_name, last_name, email, password } = req.body;

    // Validate user input
    if (!(email && password && first_name && last_name)) {
      res.status(400).send("All input is required");
    }

    // check if user already exist
    // Validate if user exist in our database
    const oldUser = await User.findOne({ email });

    if (oldUser) {
      return res.status(409).send("User Already Exist. Please Login");
    }

    //Encrypt user password
    encryptedPassword = await bcrypt.hash(password, 10);

    // Create user in our database
    const user = await User.create({
      first_name,
      last_name,
      email: email.toLowerCase(), // sanitize: convert email to lowercase
      password: encryptedPassword,
    });
    // Create token
    const token = jwt.sign(
      { user_id: user._id, email },
      process.env.TOKEN_KEY,
      {
        expiresIn: "2h",
      }
    );
    // save user token
    user.token = token;

    // return new user
    res.status(201).json(user);
  } catch (err) {
    console.log(err);
  }
  // Our register logic ends here
};
exports.login = async (req, res) => {
  // our login logic goes here
  try {
    // Get user input
    const { email, password } = req.body;

    // Validate user input
    if (!(email && password)) {
      res.status(400).send("All input is required");
    }
    // Validate if user exist in our database
    const user = await User.findOne({ email });

    if (user && (await bcrypt.compare(password, user.password))) {
      // Create token
      const token = jwt.sign(
        { user_id: user._id, email },
        process.env.TOKEN_KEY,
        {
          expiresIn: "2h",
        }
      );

      // save user token
      user.token = token;

      // user
      res.status(200).json(user);
    }
    res.status(400).send("Invalid Credentials");
  } catch (err) {
    console.log(err);
  }
  // Our register logic ends here
};
exports.welcome = (req, res) => {
  res.status(200).send("Welcome 🙌 ");
};

