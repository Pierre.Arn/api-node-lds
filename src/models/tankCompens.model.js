const mongoose = require('mongoose');

const tankCompens = new mongoose.Schema({
  TankNumber: Number,
  TankCompensVolume: Number,
  TankCompensHeight: Number,
  TankCompensConcentrationPotential: Number,
  TankCompensConcentrationDesired: Number,
  TankCompensDensity: Number,
  TankCompensTempFreezing: Number,
  TankCompensVolumeSW: Number,
  TankCompensSaltWeight: Number,
  TankCompensBags: Number,
});

module.exports = mongoose.model('Tanks_Compens', tankCompens, 'Tanks_Compens');