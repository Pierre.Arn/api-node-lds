const mongoose = require('mongoose');

const config = new mongoose.Schema({
  BoatName: String,
  NumberOfTanks: Number,
  OddTanks: String,
  SvgHeight: Number,
  SvgWidth: Number,
  SvgPath: String,
});

module.exports = mongoose.model('Configs', config, 'Configs');