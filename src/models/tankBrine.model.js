const mongoose = require('mongoose');

const tankBrine = new mongoose.Schema({
  TankNumber: Number,
  TankBrineVolume: Number,
  TankBrineHeight: Number,
  TankBrineConcentration: Number,
  TankBrineDensity: Number,
  TankBrineTempFreezing: Number,
  TankBrineVolumeSW: Number,
  TankBrineSaltWeight: Number,
  TankBrineBags: Number,
});

module.exports = mongoose.model('Tanks_Brine', tankBrine, 'Tanks_Brine');