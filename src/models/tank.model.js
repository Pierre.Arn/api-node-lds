const mongoose = require('mongoose');

const tank = new mongoose.Schema({
  TankNumber: Number,
  TankName: String,
  TankVolume: Number,
  TankVolumeMax: Number,
  Fish: Boolean,
  TankState: String,
  SvgPath: String,
});

module.exports = mongoose.model('Tanks', tank, 'Tanks');