require("./config_databases/mongo.js").connect();
require("./config_databases/influx.js").connect();
require("dotenv").config();

const express = require("express");
const app = express();

const cors = require("cors");
const favicon = require('serve-favicon');

// API routes definitions
const apiM = require('./routes/influx.routes')
const apiI = require('./routes/mongo.routes')

// Middleware 
app
  .use(favicon(__dirname + '/favicon.png'))
  .use(cors())
  .use(express.urlencoded({ extended: true }))
  .use(express.json())
  .use('/api', apiM)
  .use('/api', apiI);


const { API_PORT } = process.env;
const port = process.env.PORT || API_PORT;

app.listen(port, () => {
  console.log(`Notre application est démarrée sur : http://localhost:${port}`)
});